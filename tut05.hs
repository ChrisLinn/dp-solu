-- Q1
maybeApply :: (a -> b) -> Maybe a -> Maybe b
maybeApply f Nothing = Nothing
maybeApply f (Just x) = Just (f x)


-- Q2
zWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zWith f (a:as) (b:bs) = (f a b):(zWith f as bs)


-- Q3
linearEqn :: Num a => a -> a -> [a] -> [a]
linearEqn a b lst = map (\x->a*x+b) lst


-- Q4
sqrtPM :: (Floating a, Ord a) => a -> [a]
sqrtPM x
  | x  > 0    = let y = sqrt x in [y, -y] 
  | x == 0    = [0]
  | otherwise = []

allSqrts :: (Floating a, Ord a) => [a] -> [a]
allSqrts lst = foldl (++) [] (map sqrtPM lst)

allSqrts' :: (Floating a, Ord a) => [a] -> [a]
allSqrts' lst = concatMap sqrtPM lst

allSqrts'' :: (Floating a, Ord a) => [a] -> [a]
allSqrts'' lst = concat $ map sqrtPM lst

-- Q5
sqrt_pos :: (Ord a, Floating a) => [a] -> [a]
sqrt_pos ns = map sqrt $ filter (>0) ns

sqrt_pos' :: (Ord a, Floating a) => [a] -> [a]
sqrt_pos' (n:ns)
    | n>0    = (sqrt n):(sqrt_pos' ns)
    | otherwise = sqrt_pos' ns
