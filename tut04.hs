-- Q1
data Tree a = Empty | Node (Tree a) a (Tree a)

treesort::  Ord a => [a] -> [a]
treesort ts = inorder $ lst_to_bst ts

inorder :: Tree a -> [a]
inorder Empty = []
inorder (Node l v r) = inorder l ++ [v] ++ inorder r

lst_to_bst :: Ord a => [a] -> Tree a
lst_to_bst [] = Empty 
lst_to_bst (t:ts) = insertNode t (lst_to_bst ts) 

insertNode :: Ord a => a -> Tree a -> Tree a
insertNode i Empty = Node Empty i Empty
insertNode i (Node l v r)
    | i<v     = Node (insertNode i l) v r
    | otherwise = Node l v (insertNode i r)

-- Q2
transpose :: [[a]] -> [[a]] 
transpose [] = error "transpose of zero-height matrix"
transpose list@(xs:xss)
    | len>0             = transpose' len list
    | otherwise         = error "transpose of zero-width matirix"
    where len = (length xs)

transpose' :: Int -> [[a]] -> [[a]]
transpose' len [] = replicate len []
transpose' len list@(xs:xss)
    | len == (length xs) = myZipWith (:) xs (transpose' len xss)
    | otherwise         = error "transpose of non-rectangular matrix"

myZipWith :: (a->b->c) -> [a] -> [b] -> [c]
myZipWith _ [] [] = []
myZipWith _ [] (_:_) = error "zipWith: list length mismatch"
myZipWith _ (_:_) [] = error "zipWith: list length mismatch" -- obviously there is something wrong with the workshop solution
myZipWith f (x:xs) (y:ys) = (f x y):(myZipWith f xs ys)


-- Q3
stats :: [Int] -> (Int, Int, Int)
stats ns = ((length ns), (sum ns), (sum $ map (\x->x^2) ns))

stats' :: [Int] -> (Int, Int, Int)
stats' ns = foldl step (0,0,0) ns
                where step (last1,last2,last3) n = ((last1+1),(last2+n),(last3+(n^2))) 

stats'' :: [Int] -> (Int, Int, Int)
stats'' ns = foldr step (0,0,0) ns
                where step n (last1,last2,last3) = ((last1+1),(last2+n),(last3+(n^2)))

stats''' :: [Int] -> (Int, Int, Int)
stats''' [] = (0,0,0)
stats''' (n:ns) = 
                    let (l,s,ss) = stats''' ns
                    in (l+1,s+n,ss+(n^2))
