-- Q5
myHead :: [a] -> a
myHead [] = error "cao ni ma da xie keng bu neng yong yu kong list"
myHead (e:es) = e

myTail :: [a] -> [a]
myTail [] = error "cao ni ma da xie keng bu neng yong yu kong list"
myTail (e:es) = es

-- Q6
--  v1
append' :: [a] -> [a] -> [a]
append' [] ys = ys
append' xs ys = append' (init xs) ((last xs):ys)
--  v2
append'' :: [a] -> [a] -> [a]
append'' [] lst = lst
append'' (e:es) lst = e:(append'' es lst)

-- Q7
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (e:es) = (myReverse es) ++ [e]

-- Q8
getNthElem :: Int -> [a] -> a
getNthElem 1 (x:xs) = x
getNthElem n [] = error "zhe zhao dui wo bu guan yong a"
getNthElem n (x:xs)
    | n<1 = error "wo le ge cao ni ge biao zi"
    | otherwise = getNthElem (n-1) xs