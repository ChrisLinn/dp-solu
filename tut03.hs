-- Q2
ftoc :: Double -> Double
ftoc f = (5/9) * (f - 32)

-- Q3
quadRoots :: Double -> Double -> Double -> [Double]
quadRoots 0 0 _ = error "Either a or b must be non-zero"
quadRoots 0 b c = [-c / b]
quadRoots a b c
    | disc < 0  = error "No real solutions"
    | disc == 0 = [tp]
    | disc > 0  = [tp + temp, tp - temp]
    where   disc = b*b - 4*a*c
            temp = sqrt(disc) / (2*a)
            tp   = -b / (2*a)

-- Q4
mymerge :: Ord a => [a] -> [a] -> [a]
mymerge [] bs = bs
mymerge as [] = as
mymerge (a:as) (b:bs)
    | a<b   = (a:(mymerge as (b:bs)))
    | otherwise = (b:(mymerge (a:as) bs))

-- Q5
qsort1 :: Ord a => [a] -> [a]
qsort1 [] = []
qsort1 (p:ps) = (qsort1 (filter (<p) ps)) ++ [p] ++ (qsort1 (filter (>p) ps))

-- try mergeSort
mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort (xs) = mergeSort lesser ++ [pivot] ++ mergeSort greater
    where
    lesser  = filter (< pivot)  xs
    greater = filter (> pivot) xs
    pivot = xs!!((length xs) `div` 2)

-- Q6
data Tree k v = Leaf | Node k v (Tree k v) (Tree k v) -- cannot "data Tree k v = Leaf | Node k v Tree Tree"
        deriving (Eq, Show)       
        
same_shape :: (Tree k v) -> (Tree k v) -> Bool -- cannot "Tree -> Tree -> Bool"
same_shape (Node _ _ _ _) Leaf = False
same_shape Leaf (Node _ _ _ _) = False
same_shape Leaf Leaf = True
same_shape (Node _ _ a b) (Node _ _ c d) = (same_shape a c) && (same_shape b d)

-- Q7
data Expression
       = Var Variable
       | Num Integer
       | Plus Expression Expression
       | Minus Expression Expression
       | Times Expression Expression
       | Div Expression Expression

data Variable = A | B

exp1 = Plus (Times (Num 2) (Var A)) (Var B)

eval :: Integer -> Integer -> Expression -> Integer
eval i1 i2 (Plus e1 e2) = (eval i1 i2 e1) + (eval i1 i2 e2)
eval i1 i2 (Minus e1 e2) = (eval i1 i2 e1) - (eval i1 i2 e2)
eval i1 i2 (Times e1 e2) = (eval i1 i2 e1) * (eval i1 i2 e2)
eval i1 i2 (Div e1 e2) = (eval i1 i2 e1) `div` (eval i1 i2 e2)
eval i1 i2 (Var A) = i1
eval i1 i2 (Var B) = i2
eval i1 i2 (Num n) = n
