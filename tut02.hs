-- Q2
data Font = Font Size Face Color
type Size = Int
type Face = String
data Color = RGB Int Int Int

-- Q3
fac :: Int -> Int
fac 1 = 1
fac n = n * (fac (n-1))

-- Q4
myElem :: Eq a => a -> [a] -> Bool
myElem _ [] = False
myElem x (y:ys) -- notice don't do "myElem x (y:ys)"
    | x==y      = True
    | otherwise = myElem x ys

-- Q5 well I didn't manage to make it
longestPrefix :: Eq a => [a] -> [a] -> [a]
longestPrefix [] _ = []
longestPrefix _ [] = []
longestPrefix (x:xs) (y:ys)
    | x==y      = x:(longestPrefix xs ys)
    | otherwise = []

-- Q6
-- this is mine
maccarthy_91 :: Int -> Int
maccarthy_91 n = dowhile n 1
dowhile n c
    | (c/=0 && n>100) = dowhile (n-10) (c-1)
    | (c/=0 && n<=100) = dowhile (n+11) (c+1)
    | otherwise = n
-- this is the given solution
maccarthy_91' :: Int -> Int
maccarthy_91' n = mywhile (1,n)
mywhile (c,n) = if (c/=0) then mywhile (nextversion (c,n))
                else n
nextversion (c,n) = if n>100 then (c-1,n-10)
                    else (c+1,n+11)

-- Q7
-- forwards
range_list :: Int -> Int -> [Int]
range_list mi ma
    | mi < ma   = mi:(range_list (mi+1) ma)
    | otherwise = [mi]

-- backwards
range_list' :: Int -> Int -> [Int]
range_list' mi ma
    | mi<ma     = (range_list (mi) (ma-1)) ++ [ma]
    | otherwise = [mi]