%% Q1. can "listof(X,[1,1,1])." but not "listof(1,X)."
listof(_, []).
listof(Elem,[Elem|Rest]) :-
    listof(Elem,Rest).


%% Q2
all_same(List) :-
    all_same(_,List).


%% Q3
adjacent(E1,E2,List) :-
    append(_,[E1,E2|_],List). %% cannot be "append(_,[E1|E2|_],List)." 


%% Q4  WTF, MGJ, 估计也就不带变量的可以吧
adjacent2(E1, E2, [E1,E2|_]).
adjacent2(E1, E2, [_|Tail]) :-
    adjacent2(E1, E2, Tail).


%% Q5  WTF, MGJ, 估计也就不带变量的可以吧
before(E1, E2, [E1|List]) :-
    member(E2, List).
before(E1, E2, [_|List]) :-
    before(E1, E2, List).


%% Q6 bu hui zuo
intset_member(N,tree(_,N,_)).
intset_member(N,tree(L,N0,_)) :-
    N < N0,
    intset_member(N,L).
intset_member(N,tree(_,N0,R)) :-
    N > N0,
    intset_member(N,R).

intset_insert(N,empty,tree(empty,N,empty)).
intset_insert(N,tree(L,N,R),tree(L,N,R)).
intset_insert(N,tree(L0,N0,R),tree(L,N0,R)) :-
    N < N0,
    intset_insert(N,L0,L).
intset_insert(N,tree(L,N0,R0),tree(L,N0,R)) :-
    N > N0,
    intset_insert(N,R0,R).