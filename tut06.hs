module Main where
import Data.Char (isDigit, digitToInt)

-- Q1
maybe_tail :: [a] -> Maybe [a]
maybe_tail [] = Nothing
maybe_tail (_:xs) = Just xs

maybe_drop' :: Int -> [a] -> Maybe [a]
maybe_drop' 0 x = Just x
maybe_drop' n xs =
                    case mt of
                            Nothing -> Nothing
                            Just xs1 -> maybe_drop' (n-1) xs1
                    where mt = maybe_tail xs

maybe_drop'' :: Int -> [a] -> Maybe [a]
maybe_drop'' 0 x = Just x
maybe_drop'' n xs = maybe_tail xs
                    >>=
                    maybe_drop'' (n-1)

maybe_drop''' :: Int -> [a] -> Maybe [a]
maybe_drop''' 0 x = Just x
maybe_drop''' n xs =    maybe_tail xs
                        >>=
                        \t -> maybe_drop''' (n-1) t

maybe_drop'''' :: Int -> [a] -> Maybe [a]
maybe_drop'''' 0 x = Just x
maybe_drop'''' n xs = do
                        lol <- (maybe_tail xs)
                        maybe_drop'''' (n-1) lol

maybe_drop''''' :: Int -> [a] -> Maybe [a]
maybe_drop''''' 0 x = Just x
maybe_drop''''' n xs = do
                        maybe_tail xs
                        >>=
                        maybe_drop''''' (n-1)

maybe_drop'''''' :: Int -> [a] -> Maybe [a]
maybe_drop'''''' 0 x = Just x
maybe_drop'''''' n xs = do
                        maybe_tail xs
                        >>=
                        \t -> maybe_drop'''''' (n-1) t


-- Q2
data Tree a = Empty | Node (Tree a) a (Tree a)

print_tree :: Show a => Tree a -> IO ()
print_tree Empty = return ()
print_tree (Node l v r) = do
                            print_tree l
                            print v
                            print_tree r

print_tree' :: Show a => Tree a -> IO ()
print_tree' Empty = putStr ""
print_tree' (Node l v r) = do
                            print_tree' l
                            putStrLn $ show v
                            print_tree' r


-- Q3
str_to_num :: String -> Maybe Int
str_to_num [] = Nothing
str_to_num (d:ds) = str_to_num_acc 0 (d:ds)

str_to_num_acc :: Int -> String -> Maybe Int
str_to_num_acc val [] = Just val
str_to_num_acc val (d:ds) =
   if isDigit d then str_to_num_acc (10*val + digitToInt d) ds
   else Nothing


-- Q4
sum_line :: IO Int
sum_line = do 
                line <- getLine
                case (str_to_num line) of
                    Nothing -> return 0
                    Just n  -> do 
                                    sum_line
                                    >>=
                                    \s -> return (n+s)

sum_line' :: IO Int
sum_line' = do 
                line <- getLine
                case (str_to_num line) of
                    Nothing -> return 0
                    Just n  -> do 
                                    s <- sum_line'
                                    return (n+s)


